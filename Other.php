<?php
    ob_start();
    session_start();
    if (isset($_SESSION['user'])) {
       
    }
    /*else{
         header("Location: login.php");
    }*/

    require_once($_SERVER['DOCUMENT_ROOT'].'/model/CategoryDb.php');
    require_once($_SERVER['DOCUMENT_ROOT'].'/model/ProductDb.php');
    require_once($_SERVER['DOCUMENT_ROOT'].'/model/OtherDb.php');
    
    $ot = new OtherDb();
    $cat = new CategoryDb();
    $pro = new ProductDb();
   
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Shop Homepage - Start Bootstrap Template</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/shop-homepage.css" rel="stylesheet">
    <style>
         body  {
            background-image: url("../css/images/header.jpg");
            background-color: #cccccc;
         }
    </style>

</head>

<body>
        <style>
             p{
             text-align: center;
             color: white;
              font-size: 40px;
             
         }
         th{
            color: white;
         }
        </style>
                     
        
                   
                    
            
                                            


            <table class="table table-striped" >
                    <thead>
                        <tr>
                            <td colspan="7">
                                <a href="index.php" class="btn btn-primary">Quay Lai</a>
                                <p style="text-align: center;">Danh Sách Other</p>
                                
                            </td>
                        </tr>
                        <tr>
                            <th>Mã Phòng</th>
                            
                            <th>Số lượng phòng</th>
                            <th>Số Other</th>
                            <th>Khách hàng</th>
                            <th>Time Other</th>
                            <th>Số ngày thuê</th>
                            <th>Số Cmnd</th>
                            
                        </tr>
                    </thead>

                        <?php
                            setlocale(LC_MONETARY,"en_US");
                            $otherB = $ot->getother()->data;
                            foreach ($otherB as $item){

                                $id = $item['id'];
                                $produc_id = $item['product_id'];
                                $quantity = $item['quantity'];
                                $bill_id = $item['bill_id'];
                                $khachhang = $item['khachhang'];
                                $ID_Bill = $item['ID_Bill'];
                                $ngaythue = $item['ngaythue'];
                                $cmnd = $item['cmnd'];
                            

                        ?>
                    <tbody>
                        <tr>
                            <th><?= $id?></th>
                             <th><?= $quantity?></th>
                            <th><?= $bill_id?></th>
                            <th><?= $khachhang ?></th>
                            <th><?=$ID_Bill?></th>
                            <th><?=$ngaythue?></th>
                            <th><?=$cmnd?></th>
                            
                        </tr>

              
            <?php
            }
        ?>

                    </tbody>
            </table>

                  
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
