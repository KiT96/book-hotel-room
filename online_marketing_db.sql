-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th5 12, 2018 lúc 06:50 CH
-- Phiên bản máy phục vụ: 10.1.21-MariaDB
-- Phiên bản PHP: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `online_marketing_db`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `account`
--

CREATE TABLE `account` (
  `phone` varchar(20) CHARACTER SET utf8 NOT NULL,
  `pass` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `name` varchar(10) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `account`
--

INSERT INTO `account` (`phone`, `pass`, `name`) VALUES
('admins', '123456', 'admin'),
('uuu', '123', 'uuu');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `bill`
--

CREATE TABLE `bill` (
  `id` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `bill_details`
--

CREATE TABLE `bill_details` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `bill_id` int(11) NOT NULL,
  `khachhang` varchar(20) CHARACTER SET utf8 NOT NULL,
  `ID_Bill` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ngaythue` int(11) DEFAULT NULL,
  `cmnd` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `bill_details`
--

INSERT INTO `bill_details` (`id`, `product_id`, `quantity`, `bill_id`, `khachhang`, `ID_Bill`, `ngaythue`, `cmnd`) VALUES
(5, 1, 1, 1, 'io', '2018-05-12 19:08:59', 2, NULL),
(3, 1, 1, 1, 'nhat', '2018-05-12 19:12:38', 3, 206454),
(5, 1, 1, 1, 'Thai', '2018-05-12 22:09:49', 8, 123456),
(11, 1, 1, 1, 'hai', '2018-05-12 22:10:03', 3, 565465);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `note` varchar(30) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `category`
--

INSERT INTO `category` (`id`, `name`, `note`) VALUES
(1, 'Gường đơn(SGL)', ''),
(2, 'Gường đơn đôi(TWM)', ''),
(3, 'Gường đôi(DBL)', ''),
(4, 'EXT', ''),
(5, '3 Gường(TRPL)', '');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `description` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `vote` int(11) NOT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `product`
--

INSERT INTO `product` (`id`, `name`, `price`, `description`, `vote`, `image`, `type`) VALUES
(1, 'TP.HCM-Hotel-Mai-Linh', 800000, '0', 4, 'images/S_1.jpg', 1),
(2, 'Đà-Nẵng-Hotel-My', 700000, '1', 3, 'images/S_2.jpg', 1),
(3, 'HàNội-Hotel-Lan-Anh', 650000, '0', 4, 'images/S_3.jpg', 1),
(4, 'TP.HCM-Hotel-Mai-Linh', 750000, '1', 2, 'images/S_4.jpg', 1),
(5, 'Đà-Nẵng-Hotel-My', 1200000, '0', 2, 'images/TWN_1.jpg', 2),
(6, 'HàNội-Hotel-Lan-Anh', 1500000, '0', 5, 'images/TWN_2.jpg', 2),
(7, 'TP.HCM-Hotel-Mai-Linh', 2500000, '0', 5, 'images/TWN_3.jpg', 2),
(8, 'NhaTrang-Hotel-SunRise', 1250000, '0', 3, 'images/TWN_4.jpg', 2),
(9, 'Đà-Nẵng-Hotel-My', 1800000, '0', 3, 'images/DBL_1.jpg', 3),
(10, 'TP.HCM-Hotel-Mai-Linh', 2500000, '0', 3, 'images/DBL_2.jpg', 3),
(11, 'HàNội-Hotel-Lan-Anh', 2300000, '0', 3, 'images/DBL_3.jpg', 3),
(12, 'NhaTrang-Hotel-SunRise', 4300000, '0', 3, 'images/DBL_4.jpg', 3),
(13, 'HàNội-Hotel-Dulde', 2500000, '0', 3, 'images/TRPL_1.jpg', 5),
(14, 'HàNội-Hotel-Dulde', 1900000, '0', 3, 'images/TRPL_2.jpg', 5),
(15, 'HàNội-Hotel-Dulde', 2100000, '1', 4, 'images/TRPL_3.jpg', 5),
(16, 'HàNội-Hotel-Dulde', 2200000, '0', 3, 'images/TRPL_4.jpg', 5),
(17, 'BinhDuong-Hotel-Duong', 1200000, '0', 4, 'images/EXT_1.jpg', 4),
(18, 'BinhDuong-Hotel-Duong', 1300000, '0', 4, 'images/EXT_2.jpg', 4);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`name`);

--
-- Chỉ mục cho bảng `bill`
--
ALTER TABLE `bill`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `bill_details`
--
ALTER TABLE `bill_details`
  ADD PRIMARY KEY (`ID_Bill`);

--
-- Chỉ mục cho bảng `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
