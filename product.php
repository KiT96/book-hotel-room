<?php
    session_start();

    require_once($_SERVER['DOCUMENT_ROOT'].'/model/CategoryDb.php');
    require_once($_SERVER['DOCUMENT_ROOT'].'/model/ProductDb.php');
    $cat = new CategoryDb();
    $pro = new ProductDb();

    $id = $_GET['cat'];
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Shop Homepage - Start Bootstrap Template</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/shop-homepage.css" rel="stylesheet">
    <style>
         body  {
            background-image: url("../css/images/header.jpg");
            background-color: #cccccc;
         }
    </style>

</head>

<body>
 <style type="text/css">
        .bg-dark {
            background-color: #adb!important;
            height: 60px;
        }
    </style>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="index.php">AGODO</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">

                <li class="nav-item active">
                    <a class="nav-link" href="index.php">Trang Chủ
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <?php

                    if (isset($_SESSION['user'])) {

                        $user = $_SESSION['user'];

                        ?>
                                <li class="nav-item">
                                    <a class="nav-link" href="#"><?= $user['name']?></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="logout.php">Logout</a>
                                </li>
                        <?php

                    }else{
                        ?>
                            <li class="nav-item">
                                <a class="nav-link" href="login.php">Login</a>
                            </li>
                        <?php
                    }
                ?>

            </ul>
        </div>
    </div>
</nav>

<!-- Page Content -->
<div class="container" style="min-height: 800px;">

    <div class="row">

        <div class="col-lg-3">

            <h1 class="my-4" style="color: white;">Phòng
            </h1>
            <div class="list-group">
                <?php
                    $categories = $cat->getAll()->data;
                    foreach ($categories as $item){
                        ?>
                            <a href="product.php?cat=<?= $item['id']?>" class="list-group-item"><?= $item['name']?></a>
                        <?php
                    }

                ?>
            </div>

        </div>
        <!-- /.col-lg-3 -->

        <div class="col-lg-9">



            <div class="row">
                <?php
                    setlocale(LC_MONETARY,"en_US");
                    $products = $pro->getProductById($id)->data;

                    foreach ($products as $item){
                        $id = $item['id'];
                        $name = $item['name'];
                        $image = $item['image'];
                        $price = $item['price'];
                        $description = $item['description'];
                        $vote = $item['vote'];
                        $type = $item['type'];

                        ?>
                            <div class="col-lg-4 col-md-6 mb-4">
                                <div class="card h-100">
                                    <a href="#"><img class="card-img-top" src="<?= $image?>" alt=""></a>
                                    <div class="card-body">
                                        <h4 class="card-title">
                                            <a href="#"><?= $name?></a>
                                        </h4>
                                        <h5 style="color: #f47442"><?= number_format($price,0);?></h5>
                                         
                                   
                                        <small class="text-muted">
                                            <?php
                                                for ($i = 1; $i <= $vote; $i++ ){
                                                    echo "&#9733";
                                                }
                                                for ($i = 1; $i <= (5-$vote); $i++ ){
                                                    echo "&#9734";
                                                }
                                            ?>
                                        </small>
                                    </div>
                                    <div class="card-footer">
                                        <form method="post" action="cart.php">
                                            <input type="hidden" name="id" value=<?=$id?>>
                                             <input type="hidden" name="name" value=<?=$name?>>
                                              <input type="hidden" name="image" value=<?=$image?>>
                                               <input type="hidden" name="price" value=<?=$price?>>
                                                <input type="hidden" name="description" value=<?=$description?>>
                                                 <input type="hidden" name="vote" value=<?=$vote?>>
                                                 <input type="hidden" name="type" value=<?=$type?>>

                                            <input type="hidden" name="data" value=<?= str_replace("\"","'",json_encode($item))?>>

                                            <button type="submit" class="btn btn-primary">Đặt Phòng</button>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        <?php
                    }
                ?>


            </div>
            <!-- /.row -->

        </div>
        <!-- /.col-lg-9 -->

    </div>
    <!-- /.row -->

</div>
<!-- /.container -->

<!-- Footer -->
<footer class="py-5 bg-dark">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Design By NguyenDuyNhat 51303120</p>
    </div>
    <!-- /.container -->
</footer>

<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
