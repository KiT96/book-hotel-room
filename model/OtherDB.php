<?php

    require_once($_SERVER['DOCUMENT_ROOT'].'/model/DbHelper.php');
    require_once($_SERVER['DOCUMENT_ROOT'].'/model/Model.php');


    class OtherDb{

        public $db;

        public function __construct()
        {
            $this->db = DbHelper::getInstance();
        }

        public function getother(){

            $response = new Response();
            try{
                $sql = "select * from bill_details";
                $param = array();

                $result = $this->db->get($sql, $param);

                if ($result == Response::$FAILED){
                    $response->status = Response::$FAILED;
                    $response->message = "Truy vấn thông tin không thành công";
                }else{
                    $response->status = Response::$SUCCESS;
                    $response->message = "Truy vấn thành công";
                    $response->data = $result;
                }
            }catch (Exception $e){

                $response->status = Response::$ERROR;
                $response->message = $e->getMessage();
            }

            return $response;
        }


    }
?>