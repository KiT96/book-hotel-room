<?php

    require_once($_SERVER['DOCUMENT_ROOT'].'/model/DbHelper.php');
    require_once($_SERVER['DOCUMENT_ROOT'].'/model/Model.php');


    class bill{

        public $db;

        public function __construct()
        {
            $this->db = DbHelper::getInstance();
        }

        public function addbill($id,$kh,$songay,$cmnd){

            $response = new Response();
            try{
                $sql = " insert into bill_details VALUES (?,1,1,1,?,default,?,?);";
                $param = array($id,$kh,$songay,$cmnd);

                $result = $this->db->get($sql, $param);

                if ($result == Response::$FAILED){
                    $response->status = Response::$FAILED;
                    $response->message = "Truy vấn thông tin không thành công";
                }else{
                    $response->status = Response::$SUCCESS;
                    $response->message = "Truy vấn thành công";
                    $response->data = $result;
                }
            }catch (Exception $e){

                $response->status = Response::$ERROR;
                $response->message = $e->getMessage();
            }

            return $response;
        }

        

    }
?>