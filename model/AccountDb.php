<?php

    require_once($_SERVER['DOCUMENT_ROOT'].'/model/DbHelper.php');
    require_once($_SERVER['DOCUMENT_ROOT'].'/model/Model.php');


    class AccountDb{

        public $db;

        public function __construct()
        {
            $this->db = DbHelper::getInstance();
        }

        public function get($phone, $pass){

            $response = new Response();
            try{
                $sql = "select * from account where phone = ? and pass = ?";
                $param = array($phone, $pass);

                $result = $this->db->get($sql, $param);

                if ($result == Response::$FAILED){
                    $response->status = Response::$FAILED;
                    $response->message = "Sai username hoặc mật khẩu";
                }else{
                    $response->status = Response::$SUCCESS;
                    $response->message = "Truy vấn thành công";
                    $response->data = $result[0];
                }
            }catch (Exception $e){

                $response->status = Response::$ERROR;
                $response->message = $e->getMessage();
            }

            return $response;
        }
        public function getallAC(){

            $response = new Response();
            try{
                $sql = "select * from account";
                $param = array();

                $result = $this->db->get($sql, $param);

                if ($result == Response::$FAILED){
                    $response->status = Response::$FAILED;
                    $response->message = "Truy vấn thông tin không thành công";
                }else{
                    $response->status = Response::$SUCCESS;
                    $response->message = "Truy vấn thành công";
                    $response->data = $result;
                }
            }catch (Exception $e){

                $response->status = Response::$ERROR;
                $response->message = $e->getMessage();
            }

            return $response;
        }
         public function addACC($phone,$pass,$name){

            $response = new Response();
            try{
                $sql = " insert into account VALUES (?,?,?);";
                $param = array($phone,$pass,$name);

                $result = $this->db->get($sql, $param);

                if ($result == Response::$FAILED){
                    $response->status = Response::$FAILED;
                    $response->message = "Truy vấn thông tin không thành công";
                }else{
                    $response->status = Response::$SUCCESS;
                    $response->message = "Truy vấn thành công";
                    $response->data = $result;
                }
            }catch (Exception $e){

                $response->status = Response::$ERROR;
                $response->message = $e->getMessage();
            }

            return $response;
        }
    }

?>