<?php

    class Product{
        public $id;
        public $name;
        public $price;
        public $desc;
        public $vote;
        public $image;
        public $type;

        public function __construct($id, $name, $price, $desc, $vote, $image, $type)
        {
            $this->id = $id;
            $this->name = $name;
            $this->$price = $price;
            $this->desc = $desc;
            $this->vote = $vote;
            $this->image = $image;
            $this->type = $type;
        }
    }

    class Account{
        public $phone;
        public $name;
        public $pass;

        public function __construct($phone, $name, $pass)
        {
            $this->phone = $phone;
            $this->name = $name;
            $this->pass = $pass;
        }
    }

    class Response{

        public static $SUCCESS = 1;
        public static $FAILED = 0;
        public static $ERROR = -1;

        public $status;
        public $message;
        public $data;

        public function __construct()
        {
        }

    }
    class Other{
        public $id;
        public $product_id;
        public $quantity;
        public $bill_id;
        public $khachhang;
        public $ID_Bill;
        public $ngaythue;
        public $cmnd;

        public function __construct($id, $product_id, $quantity, $bill_id, $khachhang, $ID_Bill, $ngaythue, $cmnd)
        {
            $this->id = $id;
            $this->product_id = $product_id;
            $this->quantity = $quantity;
            $this->bill_id = $bill_id;
            $this->khachhang = $khachhang;
            $this->ID_Bill = $ID_Bill;
            $this->ngaythue = $ngaythue;
            $this->cmnd = $cmnd;
        }
    }
?>