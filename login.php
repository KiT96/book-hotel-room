<?php
    ob_start();
    session_start();
    require_once($_SERVER['DOCUMENT_ROOT'].'/model/AccountDb.php');

    if (isset($_SESSION['user'])) {
        header("Location: index.php");
    }
    $acc = new AccountDb();
?>

<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Flat HTML5/CSS3 Login Form</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
      <link rel="stylesheet" href="css/style.css">

  
</head>

<script>
    function showError() {
        $(".alert-danger").fadeIn(1500);
        setTimeout(function () {
            $(".alert-danger").fadeOut(3000);
        },3000);
    }
</script>

<body>
  <div class="login-page">
  <div class="form">

    <form class="login-form" method="post">
      <input type="text" placeholder="Username" name="phone"/>
      <input type="password" placeholder="password" name="pass"/>
      <button>Login</button>
      <p class="message">Not registered? <a href="Create.php">Create an account</a></p>
    </form>
  </div>

      <div class="alert alert-danger" style="text-align: center; display: none">
          <span>Sai username hoặc mật khẩu</span>
      </div>

</div>
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script  src="js/index.js"></script>

  <?php

        if (isset($_POST['phone']) && isset($_POST['pass'])) {

            $phone = $_POST['phone'];
            $pass = $_POST['pass'];

            $result = $acc->get($phone, $pass);
            if ($result->status == Response::$SUCCESS) {

                //$result = json_decode(json_encode($result), True);
                $_SESSION['user'] = $result->data;
                header("Location: index.php");

            }else if ($result->status == Response::$FAILED) {
                echo "<script>showError()</script>";
            }else{
                echo "<script>showError()</script>";
            }
        }

  ?>


</body>
</html>
